# Presence - anonymous presence tracker

You can use this project if you want to track presence of users at different places.
[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![Total Downloads](https://img.shields.io/packagist/dt/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

The framework source code can be found here: [cakephp/cakephp](https://github.com/cakephp/cakephp).

## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Run `php composer.phar install`.

## Configuration

Read and edit `config/app.default.php` and setup the `'Datasources'` and any other
configuration relevant for your application. Save it as app.php.

Fire up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Run migrations and seeds

```
bin/cake migrations migrate
bin/cake migrations seed
```

