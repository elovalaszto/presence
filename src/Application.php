<?php
/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     3.3.0
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */
namespace App;

use ArrayAccess;
use Authentication\AuthenticationService;
use Authentication\AuthenticationServiceProviderInterface;
use Authentication\Middleware\AuthenticationMiddleware;
use Authorization\AuthorizationService;
use Authorization\AuthorizationServiceInterface;
use Authorization\AuthorizationServiceProviderInterface;
use Authorization\Middleware\AuthorizationMiddleware;
use Authorization\Policy\OrmResolver;
use Cake\Core\Configure;
use Cake\Core\Exception\MissingPluginException;
use Cake\Error\Middleware\ErrorHandlerMiddleware;
use Cake\Http\BaseApplication;
use Cake\Routing\Middleware\AssetMiddleware;
use Cake\Routing\Middleware\RoutingMiddleware;
use Cake\Routing\Router;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Cake\Http\Middleware\CsrfProtectionMiddleware;




use Cake\Http\Middleware\EncryptedCookieMiddleware;

/**
 * Application setup class.
 *
 * This defines the bootstrapping logic and middleware layers you
 * want to use in your application.
 */
class Application extends BaseApplication implements AuthenticationServiceProviderInterface, AuthorizationServiceProviderInterface
{
    /**
     * {@inheritDoc}
     */
    public function bootstrap()
    {


        // Call parent to load bootstrap from files.
        parent::bootstrap();
        $this->addPlugin('Authentication');
        $this->addPlugin('Authorization');
        if (PHP_SAPI === 'cli') {
            try {
                $this->addPlugin('Bake');
            } catch (MissingPluginException $e) {
                // Do not halt if the plugin is missing
            }

            $this->addPlugin('Migrations');
        }

        /*
         * Only try to load DebugKit in development mode
         * Debug Kit should not be installed on a production system
         */
        if (Configure::read('debug')) {
            $this->addPlugin('DebugKit', ['bootstrap' => true]);
        }
    }

    public function getAuthenticationService(ServerRequestInterface $request, ResponseInterface $response)
    {
        $service = new AuthenticationService();
        $service->loadIdentifier('Authentication.Password');
        $service->loadAuthenticator('Authentication.Session');
        $service->loadAuthenticator('Authentication.Form', [
            'fields' => [
                'username' => 'username',
                'password' => 'password',
            ],
            'loginUrl' => '/'
        ]);

        return $service;
    }

    public function getAuthorizationService(ServerRequestInterface $request, ResponseInterface $response)
    {
        $resolver = new OrmResolver();
        return new AuthorizationService($resolver);
    }

    /**
     * Setup the middleware queue your application will use.
     *
     * @param \Cake\Http\MiddlewareQueue $middlewareQueue The middleware queue to setup.
     * @return \Cake\Http\MiddlewareQueue The updated middleware queue.
     */
    public function middleware($middlewareQueue)
    {

        $cookies = new EncryptedCookieMiddleware(
            ['secrets', 'protected'],
            Configure::read('Security.cookieKey')
        );
        $authentication = new AuthenticationMiddleware($this, [
        'unauthenticatedRedirect' => '/users/bejelentkezes',
        'queryParam' => 'redirect',
        ]);
        $authorization = new AuthorizationMiddleware($this, [
            'identityDecorator' => function (AuthorizationServiceInterface $authorization, ArrayAccess $identity) {
                return $identity->setAuthorization($authorization);
            },
            'requireAuthorizationCheck' => true,
            'unauthorizedHandler' => [
                'className' => 'Authorization.Redirect',
                'url' => '/',
                'queryParam' => 'redirectUrl',
                'exceptions' => [
                    MissingIdentityException::class,
                    OtherException::class
                ]
            ]
        ]);


        $middlewareQueue
            ->add(new ErrorHandlerMiddleware(null, Configure::read('Error')))
            ->add(new AssetMiddleware([
                'cacheTime' => Configure::read('Asset.cacheTime')
            ]))
            ->add(new RoutingMiddleware($this, '_cake_routes_'))
            ->add($cookies)

            ->add($authentication)
            ->add(function (
                    ServerRequestInterface $request,
                    ResponseInterface $response,
                    callable $next
                ) {
                    $params = $request->getAttribute('params');
                    if ($params['action'] !== 'getimage') {
                        $csrf = new CsrfProtectionMiddleware([
                            'httpOnly' => true
                        ]);

                        // This will invoke the CSRF middleware's `__invoke()` handler,
                        // just like it would when being registered via `add()`.
                        return $csrf($request, $response, $next);
                    }

                    return $next($request, $response);
                });
            $middlewareQueue->add($authorization);
            if (Configure::read('debug')) {
                // Disable authorization for DebugKit
                $middlewareQueue->add(function ($req, $res, $next) {
                    if ($req->getParam('plugin') === 'DebugKit') {
                        $req->getAttribute('authorization')->skipAuthorization();
                    }
                    return $next($req, $res);
                });
            }
        return $middlewareQueue;
    }
}
