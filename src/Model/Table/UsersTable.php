<?php
namespace App\Model\Table;

use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\Utility\Text;
use Cake\ORM\Table;
use Cake\Core\Configure;
use Cake\Validation\Validator;
use DateTime;
use Cake\Mailer\Email;

/**
 * Users Model
 *
 * @method \App\Model\Entity\User get($primaryKey, $options = [])
 * @method \App\Model\Entity\User newEntity($data = null, array $options = [])
 * @method \App\Model\Entity\User[] newEntities(array $data, array $options = [])
 * @method \App\Model\Entity\User|bool save(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User saveOrFail(\Cake\Datasource\EntityInterface $entity, $options = [])
 * @method \App\Model\Entity\User patchEntity(\Cake\Datasource\EntityInterface $entity, array $data, array $options = [])
 * @method \App\Model\Entity\User[] patchEntities($entities, array $data, array $options = [])
 * @method \App\Model\Entity\User findOrCreate($search, callable $callback = null, $options = [])
 *
 * @mixin \Cake\ORM\Behavior\TimestampBehavior
 */
class UsersTable extends Table
{
    /**
     * Initialize method
     *
     * @param array $config The configuration for the Table.
     * @return void
     */
    public function initialize(array $config)
    {
        parent::initialize($config);

        $this->setTable('users');
        $this->setDisplayField('username');
        $this->setPrimaryKey('id');
        $this->belongsTo('Places', [
            'foreignKey' => 'place_voted',
            'joinType' => 'INNER'
        ]);

        $this->addBehavior('Timestamp');
    }

    /**
     * Default validation rules.
     *
     * @param \Cake\Validation\Validator $validator Validator instance.
     * @return \Cake\Validation\Validator
     */
    public function validationDefault(Validator $validator)
    {
        $validator
            ->scalar('id')
            ->requirePresence('username', 'create')
            ->allowEmptyString('username', false)
            ->maxLength('id', 255)
            ->add('id', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => 'Ezzel a lakcímkártyaszámmal már regisztráltak a rendszerben.']);

        $validator
            ->integer('zip')
            ->add('zip', 'custom', [
                'rule' => function ($value, $context) {
                    if (!in_array($value, [1071,1072,1073,1074,1075,1076,1077,1078])) {
                        return false;
                    } else {
                        return true;
                    }
                },
                'message' => __('Az irányítószám nem megfelelő, csak Ferencvárosi irányítószámok adhatók meg!')
            ])
            ->requirePresence('zip', 'create')
            ->allowEmptyString('zip', false);

        $validator
            ->scalar('lastletters')
            ->maxLength('lastletters', 2)
            ->requirePresence('lastletters', 'create')
            ->allowEmptyString('lastletters', false);
        $validator
            ->scalar('docnum')
            ->requirePresence('docnum', 'create')
            ->allowEmptyString('docnum', false);




        $validator
            ->date('birthdate')
            ->add('birthdate', 'custom', [
                // Emberek szempontjából értelmezhető cél
                // Kilépés: megszűnt civilnek lenni a történet
                'rule' => function ($value, $context) {
                    if (new DateTime($value['year']. '-' . $value['month']. '-' . $value['day']) < new DateTime('2001-10-01')) {
                        return true;
                    } else {
                        return false;
                    }
                },
                'message' => __('Az előválasztáson csak 2001 október 1. előtt születettek vehetnek részt')
            ])
            ->requirePresence('birthdate', 'create')
            ->allowEmptyDate('birthdate', false);

        $validator
            ->add('acceptance', 'custom', [
                'rule' => function ($value, $context) {
                    if ($value != 1){
                        return false;
                    } else {
                        return true;
                    }
                },
                'message' => __('A felhasználási feltételeket el kell fogadni a használathoz')
            ]);



        $validator
            ->boolean('voted')
            ->allowEmptyString('voted');

        $validator
            ->dateTime('vote_time')
            ->allowEmptyDateTime('vote_time');

        $validator
            ->scalar('username')
            ->maxLength('username', 255)
            ->requirePresence('username', 'create')
            ->allowEmptyString('username', false)
            ->add('username', 'unique', ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __('Válassz másik felhasználónevet, ez már foglalt!')]);

        $validator
            ->scalar('password')
            ->maxLength('password', 255)
            ->requirePresence('password', 'create')
            ->allowEmptyString('password', false);

        return $validator;
    }

    /**
     * Returns a rules checker object that will be used for validating
     * application integrity.
     *
     * @param \Cake\ORM\RulesChecker $rules The rules object to be modified.
     * @return \Cake\ORM\RulesChecker
     */
    public function buildRules(RulesChecker $rules)
    {
        $rules->add($rules->isUnique(['username']));
        return $rules;
    }


    public function canVote ($id = null) {
        $vote_start = Configure::read('vote.start');
        $vote_end = Configure::read('vote.end');
        $reasons = [];
        if (new DateTime('now') < new DateTime($vote_start)) {
            array_push($reasons, __('Még nem nyitottuk meg a szavazást, csak a regisztrációt. Szavazni {0}-tól lehet!', $vote_start));
            // return($reasons);
        }
        if (new DateTime('now') > new DateTime($vote_end)) {
            array_push($reasons, __('A szavazás már lezárásra került. Szavazni {0}-ig lehetett!', $vote_end));
            // return($reasons);
        }


        $user = $this->get($id);

        if ($user->voted) {
                $place = $this->Places->get($user->place_voted);
                array_push($reasons, __('A rendszer nyilvántartása szerint ezzel a lakcímkártyával már szavazás történt ekkor: ' . $user->vote_time . __(', itt: ' . $place->name) . '<br><strong style="color:red;">FIGYELEM! Ha ez az időpont pont most van, akkor odaadható a szavazólap!</strong>'));}
        if ($user->role != 0 ) array_push($reasons, __('Csak szavazók szavazhatnak!'));
        if ($user->birthdate > new DateTime('2001-10-01')) array_push($reasons, __('Csak 2001-10-01 előtt születettek vehetnek részt az előválasztáson.'));
        if (!empty($reasons)) {return $reasons; } else {return true; }

    }

}
