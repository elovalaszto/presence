<?php
namespace App\Policy;

use App\Model\Entity\User;
use Authorization\IdentityInterface;
use Cake\Core\Configure;
use DateTime;

/**
 * User policy
 */
class UserPolicy
{


    public function canAdmin(IdentityInterface $user) {
        if ($user->role === 3) { // Only admins can register volounteers
            return true;
        } else  {
            return false;
        }
    }

    public function canVolounteer(IdentityInterface $user) {
        if ($user->role === 1) { // Only admins can register volounteers
            return true;
        } else  {
            return false;
        }
    }

}
