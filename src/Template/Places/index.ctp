<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Place[]|\Cake\Collection\CollectionInterface $places
 */
?>
        <section class="section">
          <div class="section-header">
            <h1><?= __('Szavazóhelyek') ?></h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->


            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
                <!-- </p> -->
        <table class="table table-striped" style="-webkit-border-radius: .25rem; -moz-border-radius: .25rem; border-radius: .25rem; overflow: hidden;">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                    <th scope="col"><?= $this->Paginator->sort('name', ['label' => 'Név']) ?></th>
                    <th scope="col" class="actions"><?= __('Lehetőségek') ?></th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($places as $place): ?>
                <tr>
                    <th scope="row"><?= $this->Number->format($place->id) ?></th>
                    <td><?= h($place->name) ?></td>
                    <td class="actions">
                        <?= $this->Html->link(__('Szerkesztés'), ['action' => 'edit', $place->id], ['class' => 'btn btn-outline-info btn-sm']) ?>
                        <?= $this->Form->postLink(__('Törlés'), ['action' => 'delete', $place->id], ['class' => 'btn btn-outline-info btn-sm'], ['confirm' => __('Are you sure you want to delete # {0}?', $place->id)]) ?>
                    </td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
        <div class="buttons">
            <nav aria-label="Page navigation">
                <ul class="pagination presence" style="margin-top: 2rem;">
                    <?= $this->Paginator->first('<< ' . __('első')) ?>
                    <?= $this->Paginator->prev('< ' . __('előző')) ?>
                    <?= $this->Paginator->numbers() ?>
                    <?= $this->Paginator->next(__('következő') . ' >') ?>
                    <?= $this->Paginator->last(__('utolsó') . ' >>') ?>
                </ul>
            </nav>
        </div>
              <!-- </p> -->
              </div>
              <div class="card-footer bg-whitesmoke">
                <?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?>
              </div>
            </div>

          </div>
        </section>