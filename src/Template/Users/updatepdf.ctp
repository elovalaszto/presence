<?php
                $this->Form->setTemplates([
                    'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}} class="label-inline">{{text}}</label>',
                    'formGroup' => '{{input}}{{label}}',
                ]);

?>
        <section class="section">
          <div class="section-header">
            <h1>PDF file ellenőrzése</h1>
            <p>Ha nincs már meg a PDF, amellyel eredetileg regisztált akkor a következő a feltöltés menete:</p>
            <ul>
              <li>http://presence.hu/hitelesites<br>Bejelentkezés, oldal alján a letöltés gomb.</li>
              <li> https://niszavdh.gov.hu/index oldalon az előbbi file betallózása, bejelentkezés, aláírás, letöltés.</li>
              <li> A letöltött fájl feltöltése itt.</li>
            </ul>
          </div>
          <div class="section-body">
            <div class="card">
              <div class="card-header">
                <?php echo $this->Form->create($user, ['type' => 'file']); ?>
                <?php echo $this->Form->control('submittedfile', ['type' => 'file', 'label' => 'Pdf file tallózása és feltöltése', 'required' => 'required' ]); ?>
              </div>
              <div class="card-body">
                <?php 
                echo $this->Form->control('acceptance', ['label' => 'Kifejezetten hozzájárulok személyes adataim kezeléséhez az alábbi tájékoztató (<a href="/pages/adatkezeles" target="_blank">előválasztó GDPR</a> és <a href="https://ahang.hu/adatkezeles" target="blank">Magyarhang GDPR</a>) elolvasása és elfogadása után.', 'escape' => false]);
                echo $this->Form->button(__('Pdf adatok feltöltése ellenőrzésre'), ['class' => 'btn btn-primary', 'style' => 'text-align: center; margin-top: 10px; width: 100%; display: inline-block;']) ;
                echo $this->Form->end();

                 ?>
              </div>

            </div>
          </div>
        </section>