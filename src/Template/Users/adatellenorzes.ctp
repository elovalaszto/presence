        <section class="section">
          <div class="section-header">
            <h1>Felhasználói adatok</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
                <?php if (isset($user)): ?>
                  <?php foreach ($user as $user): ?>
            <div class="card">
                  <div class="card-body">

                    <p><?php echo __('Lakcmkártya vissza nem fejthető azonosítója:') ?> <code><?php echo $user->id ?></code></p>
                    <p><?php echo __('Irányítószám:') ?> <code><?php echo $user->zip ?></code></p>
                    <p><?php echo __('Születési dátum:') ?> <code><?php echo $user->birthdate ?></code></p>
                    <p><?php echo __('Regisztráció dátuma:') ?> <code><?php echo $user->created ?></code></p>
                    <p><?php echo __('Utolsó módosítás dátuma:') ?> <code><?php echo $user->modified ?></code></p>
                    <p><?php echo __('Lakcímkártyaszám utolsó két jegye:') ?> <code><?php echo $user->lastletters ?></code></p>
                    <p><?php echo __('E-mail cím:') ?> <code><?php echo $user->anonymized ? __('Nincs adat') : $user->email ?></code></p>
                    <p><?php echo __('Mobilszám:') ?> <code><?php echo $user->mobile ?></code></p>
                    <p><?php echo __('Küldhető neki rendszerüzeneten kívül e-mail?') ?> <code><?php echo $user->email_marketable ? __('Igen') : __('Nem') ?></code></p>
                    <?php if ($user->voted): ?>
                      <p><?php echo __('Leszavazott-e már?') ?> <code><?php echo $user->voted ? __('Igen') : __('Még nem') ?></code></p>  
                      <p><?php echo __('Mikor szavazott?') ?> <code><?php echo $this->Time->format($this->Identity->get('vote_time'),  \IntlDateFormatter::FULL,  null,  'Europe/Budapest') ?></code></p>  
                      <p><?php echo __('Hol szavazott?') ?> <code><?php echo $place->name ?></code></p>  
                      <?php endif ?>
                    
                    <p><?php echo __('PIN ellenőrzésre került:') ?> <code><?php echo $user->pin_ok ? __('Igen') : __('Még nem') ?></code></p>
                  </div>
                </div>
                  <?php endforeach ?>

                <?php else :  ?>
                  <div class="card"></div>
                  <div class="card-body">

                  <?php 
                    echo $this->Form->create($user);
                    echo $this->Form->control('docnum', ['label' => 'Lekérés lakcímkártyaszám alapján', 'class' => 'form-control', 'required' => false]);
                    echo $this->Form->button('Keresés',['class' => 'btn btn-primary', 'style' => 'margin-top: 10px;']);
                    echo $this->Form->end();
                   ?>
                   <?php endif ?>
                 </div></div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>
        </section>