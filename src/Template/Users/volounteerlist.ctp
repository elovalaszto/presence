<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\User[]|\Cake\Collection\CollectionInterface $users
 */
?>
<section class="section">
          <div class="section-header">
            <h1><?= __('Önkéntesek') ?></h1>
          </div>
          <div class="section-body">

        <table class="table table-striped" style="-webkit-border-radius: .25rem; -moz-border-radius: .25rem; border-radius: .25rem; overflow: hidden;">
            <thead>
                <tr>
                    <th scope="col"><?= $this->Paginator->sort('username', ['label' => 'Felhasználónév']) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('created', ['label' => 'Létrehozva']) ?></th>
                    <th scope="col"><?= $this->Paginator->sort('modified', ['label' => 'Módosítva']) ?></th>
                    <th scope="col">Lehetőségek</th>
                </tr>
            </thead>
            <tbody>
                <?php foreach ($users as $user): ?>
                <tr>
                    <td><?= h($user->username) ?></td>
                    <td><?= h($user->created) ?></td>
                    <td><?= h($user->modified) ?></td>
                    <td><?php echo $this->Form->postLink('Jelszó újragenerálás', ['controller' => 'users', 'action' =>'regenVolounteerPass'], ['data' => ['id' => $user->id]]) ?></td>
                </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    
</section>
