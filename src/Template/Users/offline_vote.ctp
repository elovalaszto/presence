<?php 

                $this->Form->setTemplates([
                'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}} class="label-inline">{{text}}</label>',
                'formGroup' => '{{input}}{{label}}',
                ]);
 ?>


        <section class="section">
          <div class="section-header">
            <h1>Szavazó nyilvántartásba vétele</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
              	<!-- </p> -->
              	<?php
                echo $this->Form->create($user);
                echo $this->Form->control('zip', ['label' => false, 'placeholder' => 'Irányítószáma']).'<br>';
                echo $this->Form->control('docnum', ['label' => false, 'placeholder' => 'Lakcímkártya száma'] ); ?>

                
                  <p><br><code><strong>Kérlek ellenőrizd, hogy a születési dátuma korábbi, mint 2001. október 1 </strong></code></p><hr>
                  <div style="display:none;">
                  <?php echo $this->Form->control('birthdate', ['type'=> 'date', 'label' => false, 'minYear' => '1920', 'maxYear' => '2001', 'default' => new DateTime('2001-09-30')]); ?>
                    
                  </div>
                <?php 
                // echo $this->Form->control('email', ['label' => false, 'placeholder' => 'E-mail cím, ez nagyon fontos, mert csak így tudunk vele kapcsolatban maradni']).'<br>';
                // echo $this->Form->control('email_marketable', ['type'=>'checkbox', 'label' => __('Szeretne értesítéseket kapni az előválasztás fejleményeiről és az aHang kezdeményezéseiről emailben.')]).'<br><hr>';

                echo $this->Form->control('acceptance', ['label' => 'Felhasználási feltételeket és adatkezelési nyilatkozatot elfogadta. (ld. kifüggesztve a sátor falán)']);
                echo $this->Form->button('Nyilvántartásba vétel', ['class' => 'btn btn-outline-primary']);
                echo $this->Form->end();
                ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>
        </section>
<script>
  $(document).ready(function(){
    $("body").addClass('addVolounteer');
    $("body.presence.addVolounteer input").addClass('form-control');
    $("body.presence.addVolounteer select").addClass('form-control');
});  

</script>
