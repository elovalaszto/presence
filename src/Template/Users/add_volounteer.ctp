        <section class="section">
          <div class="section-header">
            <h1>Önkéntes regisztrálása</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
              	<!-- </p> -->
                <?php
                echo $this->Form->create($user);
                echo $this->Form->control('username', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => 'Felhasználónév:']);
                echo $this->Form->control('password', ['class' => 'form-control', 'style' => 'margin-bottom: 20px;', 'label' => 'Jelszó:']);
                
                $this->Form->setTemplates([
                'nestingLabel' => '{{hidden}}{{input}}<label{{attrs}} class="label-inline">{{text}}</label>',
                'formGroup' => '{{input}}{{label}}',
                ]);
                echo $this->Form->hidden('acceptance', ['label' => 'Felhasználási feltételeket és titoktartási nyilatkozatot elfogadta', 'default' => true]); ?>
                <div style="text-align: center; margin-top: 20px;"><?php echo $this->Form->button(__('Önkéntes regisztrálása'), ['class' => 'btn btn-outline-primary']); ?></div>
                <?php echo $this->Form->end();
                ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>
        </section>
<script>
  $(document).ready(function(){
    $("body").addClass('addVolounteer');
    $("body.presence.addVolounteer .input select").addClass('form-control');
});
</script>