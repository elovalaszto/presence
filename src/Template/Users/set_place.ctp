        <section class="section">
          <div class="section-header">
            <h1>Válaszd ki, hogy melyik szavazóhelyen vagy.</h1>
          </div>
          <div class="section-body">
            <!-- <h2 class="section-title">Alcím</h2>
            <p class="section-lead">Leírás.</p> -->
            <div class="card">
              <!-- <div class="card-header">
                <h4>Fej</h4>
              </div> -->
              <div class="card-body">
              	<!-- </p> -->
              	<?php echo $this->Form->create($place); ?>
              	<?php echo $this->Form->control('place_id', ['empty' => 'Válaszd ki, hogy hol vagy éppen', 'options' => $places, 'class' => 'form-control', 'label' => 'Válaszd ki, hogy melyik sátorban vagy:', 'required' => true]); ?>
              	<div style="text-align: center; margin-top: 20px;"><?php echo $this->Form->button(__('Kiválasztás'), ['class' => 'btn btn-outline-primary']); ?></div>
              	<?php echo $this->Form->end(); ?>
              <!-- </p> -->
              </div>
              <!-- <div class="card-footer bg-whitesmoke">
                Láb
              </div> -->
            </div>
          </div>
        </section>