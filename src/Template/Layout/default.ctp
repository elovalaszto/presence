<!DOCTYPE html>
<html>
<head>
    <?= $this->Html->charset() ?>
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">

    <title>
        Anonim jelenlét nyilvántartó: 
        <?= $this->fetch('title') ?>
    </title>

    <?= $this->Html->css('//fonts.googleapis.com/css?family=Catamaran:100,200,300,400,500,600,700,800,900|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i&display=swap&subset=cyrillic,cyrillic-ext,greek,greek-ext,latin-ext,tamil,vietnamese') ?>
    <?= $this->Html->css('//stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css') ?>
    <?= $this->Html->css('//use.fontawesome.com/releases/v5.7.2/css/all.css') ?>
    <?= $this->Html->css('assets/css/style.min.css') ?>
    <?= $this->Html->css('assets/css/components.min.css') ?>
    <?= $this->Html->css('style.min.css') ?>

    <?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
    <?= $this->Html->script('//code.jquery.com/jquery-3.3.1.min.js') ?>
</head>
<body class="presence <?php echo (isset($this->request->getParam('pass')[0]) && ($this->request->getParam('pass')[0] === 'home')) ? 'display' : preg_replace("/[^a-zA-Z0-9-_]/", "", $this->request->getParam('pass'[0])); ?>">
  <div id="app">
    <div class="main-wrapper">
      <div class="navbar-bg"></div>
      <nav class="navbar navbar-expand-lg main-navbar">
        <form class="form-inline mr-auto">
          <ul class="navbar-nav mr-3">
            <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
          </ul>
        </form>
      </nav>
      <div class="main-sidebar">
        <aside id="sidebar-wrapper">
          <div class="sidebar-brand" style="margin-bottom: 10px;">
            <div class="presence-logomark-animation-container" style="height: 49.61px; width: 249.97px;">

            </div>
        </div>
        <div class="sidebar-brand sidebar-brand-sm" style="margin-bottom: 10px;">
            <div class="presence-logomark-animation-container" style="height: 45px; width: 45px; top: 10px; left: 10px;">
            </div>
        </div>
        <ul class="sidebar-menu">
              <li class="menu-header">Felhasználó</li>
              <?php if ($this->Identity->isLoggedIn()): ?>
                <li>
                  <?php echo $this->Html->link('<i class="fas fa-tachometer-alt"></i>' . __('<span>Lehetőségek</span>'), ['controller' => 'users', 'action' => 'dashboard'], ['class' => 'nav-link', 'escape' => false]) ?>
                </li>
                <li>
                  <?php echo $this->Html->link('<i class="fas fa-sign-out-alt fa-flip-horizontal"></i>' . __('<span>Kijelentkezés</span>'), ['controller' => 'users', 'action' => 'kijelentkezes'], ['class' => 'nav-link', 'escape' => false]) ?>
                </li>
              <?php else : ?>   
                <li>
                  <?php echo $this->Html->link('<i class="fas fa-sign-in-alt"></i>' . __('<span>Bejelentkezés</span>'), ['controller' => 'users', 'action' => 'bejelentkezes'], ['class' => 'nav-link', 'escape' => false]) ?>
                </li>             
              <?php endif ?>
            </ul>


        </aside>
      </div>

      <!-- Main Content -->
      <div class="main-content">
        <?= $this->Flash->render() ?>
        <?= $this->fetch('content') ?>
      </div>
      <footer class="main-footer">
        <div class="footer-left">
            
        </div>
        <div class="footer-right">
          <span>Anonim jelenlét nyilvántartó<br /><br /></span>
        </div>
      </footer>
    </div>
  </div>

    <?= $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js') ?>
    <?= $this->Html->script('//stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js') ?>
    <?= $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.7.6/jquery.nicescroll.min.js') ?>
    <?= $this->Html->script('//cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js') ?>
    <?= $this->Html->script('assets/js/stisla.js') ?>
    <?= $this->Html->script('assets/js/scripts.js') ?>
    <?= $this->Html->script('assets/js/custom.js') ?>
    <?= $this->fetch('scriptbottom') ?>
</body>
</html>