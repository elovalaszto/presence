<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\Utility\Security;
use Cake\Http\Cookie\Cookie;
use Cake\Filesystem\Folder;
use Cake\Filesystem\File;
use Cake\Http\Cookie\CookieCollection;
use Cake\Utility\Xml;
use DateTime;
use Cake\Core\Configure;

/**
 * Users Controller
 *
 * @property \App\Model\Table\UsersTable $Users
 *
 * @method \App\Model\Entity\User[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class UsersController extends AppController
{

    public function beforeFilter(Event $event)
    {
        parent::beforeFilter($event);
        $this->Authentication->allowUnauthenticated(['bejelentkezes', 'metabase', 'kijelentkezes']);
    }


    public function addVolounteer() {

        $this->Authorization->authorize($this->Authentication->getIdentity(), 'admin');
        $user = $this->Users->newEntity();
        if ($this->request->is('POST')) {
            $user = $this->Users->patchEntity($user, $this->request->getData());
            $docnum = mt_rand(1000, 9999);
            $user->zip = 9999;
            $user->docnum = $docnum;
            $user->id = Security::hash($docnum, 'sha256', true);
            $user->role = 1;
            $user->birthdate = new DateTime('1979-01-01');

            $user->place_created = 1;
            $user->lastletters = substr($docnum, -2);
            $user->created_by = $this->Authentication->getIdentity()->id;
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Az önkéntes sikeresen hozzáadásra került!'));
                $this->redirect(['action' => 'dashboard']);
            }
        }
        $this->set('user', $user);
    }

    public function regenVolounteerPass() {
        $this->Authorization->authorize($this->Authentication->getIdentity(), 'admin');
        if ($this->request->is('POST')) {
            $user = $this->Users->get($this->request->getData('id'));
            $alphabet = "abcdefghijkmnopqrstuwxyz0123456789";
            for ($i = 0; $i < 8; $i++) {
                $n = rand(0, strlen($alphabet)-1);
                $pass[$i] = $alphabet[$n];
            }
            $user->password = implode($pass);
            if ($this->Users->save($user)) {
                $this->Flash->success('Új jelszó: ' . implode($pass));
            }
        }
        return $this->redirect(['action' => 'volounteerlist']);

    }

    public function createvolounteer () {
        $user = $this->Users->newEntity();
        $this->Authorization->authorize($user, 'createvolounteer'); //TODO: Authorization Policy!
        if ($this->request->is('post')) {
            $this->Users->patchEntity($user, $this->request->getData());
            $user->role = 2;
            if($result = $this->Users->save($user)) {
                $this->Flash->success(__('Sikeresen létrehoztad az önkéntest!'));
            } else {
                $this->Flash->error(__('A felhasználó létrehozása nem sikerült!'));
            }
        }
    }

    public function bejelentkezes () {

        $user = $this->Users->newEntity();
        $this->Authorization->skipAuthorization();
        $result = $this->Authentication->getResult();
        // dd($result);
        if ($result->isValid()) {
            $redirect = $this->request->getQuery('redirect', ['action' => 'dashboard']);
            return $this->redirect($redirect);
        }

        if ($this->request->is(['post']) && !$result->isValid()) {
            $this->Flash->error(__('Hibás felhasználónév vagy jelszó!'));
        }

        $this->set(compact('user'));
    }

    public function kijelentkezes () {
        $this->Authorization->skipAuthorization();
        $this->Flash->success(__('Sikeres kijelentkezés!'));
        $this->Authentication->logout();
        $this->request->getSession()->clear();
        $this->redirect('/');
        
    }

    public function setPlace() {
        $this->loadModel('Places');
        $place = $this->Places->newEntity();
        $user = $this->Authentication->getIdentity();
        $this->Authorization->authorize($user, 'volounteer');
        $places = $this->Places->find('list')->where(['id !=' =>  '1'])->order(['name' => 'ASC']); // 
        if ($this->request->is('POST')) {
            $this->request->getSession()->write('place_id', $this->request->getData('place_id'));
            $this->Flash->success(__('Sikeresen beállítottad a helyet.'));
            return $this->redirect(['action' => 'dashboard']);
        }
        $this->set(compact('places', 'place'));
    }




    public function offlineVote () {
        $user = $this->Users->newEntity();
        $this->Authorization->authorize($user, 'volounteer');

        if (!$this->request->getSession()->check('place_id')) { // csak sátorba bejelentkezve lehessen regisztrálni
            $this->redirect(['action' => 'setPlace']);
        }
        if ($this->request->is(['POST', 'PUT'])) {
            $docnum = Security::hash(strtoupper($this->request->getData('docnum')), 'sha256', true);
            // Ha már regisztrált online, de offline megy el szavazni, akkor állítsuk szavazottra
            if (0 != $this->Users->find()->where(['id' => $docnum])->count()) {
                $user = $this->Users->get($docnum);
                $reasons = $this->Users->canVote($user->id);
                if (!is_array($reasons)) {
                    $user->voted = true;
                    $user->vote_time = new DateTime('now');
                    $user->place_voted = $this->request->getSession()->read('place_id');
                    if ($this->Users->save($user)) {
                        $this->Flash->success(__('Az jelenlévő ügyféltörzset szavazottra állítottam!'));
                    } else {
                        $this->Flash->error(__('Hiba a mentés során!'));
                    }
                } else {
                    $reasontext = '';
                    foreach ($reasons as $reason) {
                        $reasontext .= $reason . '<br>';
                    }
                    $this->Flash->error($reasontext, ['escape' => false]);
                }
            } else { // most látjuk először a lakcímkártyáját
                $user = $this->Users->patchEntity($user, $this->request->getData());
                $user->id = $docnum;
                $user->place_created = $this->request->getSession()->read('place_id');
                $user->place_voted = $this->request->getSession()->read('place_id');

                $user->username = Text::uuid();
                $user->password = Text::uuid();
                $user->voted = true;
                $user->vote_time = new DateTime('now');

                $user->lastletters = substr(preg_replace('/\s+/', '', strtoupper($this->request->getData('docnum'))), -2);
                $user->role = 0;
                $user->created_by = $this->Authentication->getIdentity()->id;
                if ($result = $this->Users->save($user)) {
                    $this->Flash->success(__('<h3>OK, kérlek add oda a szavazónak a szavazólapot!<h3>'), ['escape' => false]);
                    $this->redirect(['action' => 'offlineVote']);
                } else {
                    $this->Flash->error(__('A szavazó hozzáadása nem sikerült, ellenőrizd a megadott adatokat!'));

                }

            }
        }
        $this->set('user', $user);        
    }


    public function metabase() {
        $this->Authorization->skipAuthorization();
    }

    public function dashboard () {
        $this->Authorization->skipAuthorization();
        $options = [ // minden szerepkörhöz más lehetőségek társulnak
            '1' => [
                ['label' => 'Offline szavazás', 'url' => ['action' => 'offlineVote']], 
                ['label' => 'Kijelentkezés', 'url' => ['action' => 'kijelentkezes']],

            ],
            '3' => [
                ['label' => 'Önkéntesek hozzáadása', 'url' =>['controller' => 'users', 'action' => 'addVolounteer']],
                ['label' => 'Önkéntesek listája', 'url' =>['controller' => 'users', 'action' => 'volounteerlist']],
                ['label' => 'Szavazóhelyek hozzáadása', 'url' =>['controller' => 'places', 'action' => 'add']],
                ['label' => 'Szavazóhelyek listája', 'url' =>['controller' => 'places', 'action' => 'index']],
                ['label' => 'Adatellenőrzés', 'url' =>['action' => 'adatellenorzes']],
                ['label' => 'Kijelentkezés', 'url' => ['action' => 'kijelentkezes']],

            ]
        ];
        $this->set('options', $options[$this->Authentication->getIdentity()->role]);
    }

    public function volounteerlist() {
        $this->Authorization->authorize($this->Authentication->getIdentity(), 'admin');
        $users = $this->Users->find()->where(['role' => '1']);
        $this->set('users', $users);
    }



    public function adatellenorzes() {
        $this->Authorization->authorize($this->Authentication->getIdentity(), 'admin');
        // $user = $this->Users->newEntity();
        $user = null;
        if ($this->request->is('POST')) {
            $conditions = [];
            if($this->request->getData('username') != "") {array_push($conditions, [ 'username' => $this->request->getData('username')]);}
            if($this->request->getData('signer_email_hash') != "") {array_push($conditions, [ 'signer_email_hash' => Security::hash($this->request->getData('signer_email_hash'), 'sha256', true)]);}
            if($this->request->getData('docnum') != "") {array_push($conditions, ['id' => Security::hash($this->request->getData('docnum'), 'sha256', true)]);}
            if($this->request->getData('email') != "") {array_push($conditions, ['email' => $this->request->getData('email')]);}
            
            $user = $this->Users->find()->where($conditions);
        }
        $this->set('user', $user);
    }




}
