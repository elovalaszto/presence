/**
 *
 * You can write your JS code here, DO NOT touch the default style file
 * because it will make it harder for you to update.
 *
 */

$(document).ready(function(){
    // Show modal on page load
    $("#presenceModal").modal('show');

    // Hide modal on button click
    $(".close").click(function(){
        $("#presenceModal").modal('hide');
    });

    // Paginator
    $("ul.pagination.presence li").addClass('page-item');
    $("ul.pagination.presence li a").addClass('page-link');
});